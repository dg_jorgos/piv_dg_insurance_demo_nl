<?php
require_once "includes/api.core.inc.php";
require_once "api/DataController.php";

// Called in Interaction Card JS Ajax URL
$router->add("data")
    ->controller("DataController")   // api/FooController.php
    ->action("postData")                 // Function name
    ->on("POST");

run();



/*
 * Interaction Card JS example code
 *
 *
{
    load: function () {
        function aFunction(param) {
            $.ajax({
                url: "client/project/api/foo" + param,
                type: "GET",
                dataType: "json",
                success: function(data) {

                },
                error: function() {

                }
            });
        }
    }
}
*/
