<?php

class DataController
{
    public function postData()
    {
        try {
            $data = json_decode(file_get_contents('php://input'), true);
            $guid = $data["guid"];
            $verzekering = $data["verzekering"];
            $data = array('polis_autoverzekering' => $verzekering);
            $data_json = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://innodev.dialoggroep.eu/api/demo/insurance/update-verzekering/'.$guid);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);

            $result = curl_exec($ch);
            curl_close($ch);

            header('Content-Type: application/json');
            echo $result;

            /*echo json_encode(array(
                'status' => 'success',
                'value' => $value
            ));*/

        } catch (Exception $e) {
            http_response_code(500);
            echo json_encode(array(
                'status' => 'error'
            ));
        }
    }
}
